/**
 * @description: 背景页添加如下代码：
 * 头部 import 该文件
 * 
 * 默认执行 chat.init(); 
 * 
 * 事件监听添加如下代码
 * chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
 *   chat.addChatFeedBack(request, sendResponse);
 *   return true;
 *})
 */

const domain = 'chat.openai.com';

export default {
    // 判断是否打开chatGPT页面，未打开则打开并固定
    init: () => {
        let hasOpenChat = false;
        chrome.tabs.query({}, function(tabs) {
            tabs.forEach(i => {
                if (i.url.match(domain)) {
                    hasOpenChat = true;
                }
            })
            if (!hasOpenChat) {
                chrome.tabs.create({
                    url: "https://chat.openai.com/chat",
                    pinned: true
                })
            }
        });
    },
    // 背景页监听message事件 并发往chatGPT页面请求接口数据
    addChatFeedBack: (request, sendResponse) => {
        if (request.type == 'getChatMessage') {
            chrome.tabs.query({}, function(tabs) {
                try {
                    tabs.forEach(i => {
                        if (i.url.match(domain)) {
                            chrome.tabs.sendMessage(i.id, {
                                type: 'getConversation',
                                question: request.question
                            }, res => {
                                console.log(res)
                                sendResponse(res);
                            })
                        }
                    })
                } catch {
                    chrome.storage.local.set({chatStatus: false});
                    sendResponse('发送请求失败!');
                }
            });
            return true;
        }
    }
}