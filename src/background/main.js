import chat from '@/utils/chat-back';
chat.init();

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    chat.addChatFeedBack(request, sendResponse);
    return true;
})