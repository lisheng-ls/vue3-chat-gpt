import { createApp } from 'vue'
import App from '@/content-scripts/App.vue';
import store from '@/content-scripts/store';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import chat from '@/utils/chat.js';

document.addEventListener('DOMContentLoaded', function () {
	const div = document.createElement('div');
    div.id = 'content-div';
    document.body.appendChild(div);
    createApp(App)
        .use(store)
        .use(ElementPlus)
        .mount('#content-div'); 
});

if (window.location.href.match('chat.openai.com')) {
    chrome.runtime.onMessage.addListener(function(request, sender,sendResponse) {
        chat.listenSend(request, sendResponse);
        return true;
    });
}