/**
 * @description: 注入入口文件添加如下代码：
 * 头部 import 该文件
 * 
 * 默认执行 chat.init(); 
 * 
 * chatGPT注入页面 事件监听添加如下代码
 * if (window.location.href.match('chat.openai.com')) {
 *  chrome.runtime.onMessage.addListener(function(request, sender,sendResponse) {
 *       chat.listenSend(request, sendResponse);
 *      return true;
 *   });
 *}
 * 
 * 功能页面通过以下代码请求chatGPT数据
 * chat.sendConversation(question.value).then(res => 
 *		anwser.value = res.message.content.parts[0]
 * );
 * 
 */
import { createParser } from 'eventsource-parser';

// 获取随机码
const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

// chatGPT请求公用函数
const request = (token, method, path, data)  => {
    return fetch(`https://chat.openai.com/backend-api${path}`, {
        method,
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        },
        body: data === undefined ? undefined : JSON.stringify(data),
    })
}

// 清除上次对话
export function setConversationProperty(token, conversationId, propertyObject) {
    request(token, 'PATCH', `/conversation/${conversationId}`, propertyObject)
}

const fetchModels = async (token) => {
    const resp = await request(token, 'GET', '/models').then((r) => r.json())
    return resp.models
}

// 异步迭代事件流
async function* streamAsyncIterable(stream) {
    const reader = stream.getReader()
    try {
      while (true) {
        const { done, value } = await reader.read()
        if (done) {
          return
        }
        yield value
      }
    } finally {
      reader.releaseLock()
    }
  }

export default {
    // 向背景页面发送请求message事件 用于功能页面
    sendConversation: (question) => {
        return new Promise((resolve) => {
            chrome.runtime.sendMessage({
                "type": "getChatMessage",
                "question": question
            }, function(res) {
                resolve(res);
            })
        })
    },
    // chatGPT注入页面注册该事件，用于接收由背景页面从功能页转发过来的请求
    listenSend: async (request, sendResponse) => {
        if (request.type == 'getConversation') {
            let token = null 
            try {
                const resp = await fetch('https://chat.openai.com/api/auth/session');
                const data = await resp.json().catch(() => ({}))
                token = data.accessToken;
            } catch {
                return;
            }
            let modelName;
            try {
                const models = await fetchModels()
                modelName = models[0].slug
            } catch (err) {
                modelName = 'text-davinci-002-render'
            }
            const resp = await fetch('https://chat.openai.com/backend-api/conversation', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    "action": "next",
                    "messages": [{
                        "id": uuidv4(),
                        "author": {
                            "role": "user"
                        },
                        "role": "user",
                        "content": {
                            "content_type": "text",
                            "parts": [request.question]
                        }
                    }],
                    "parent_message_id": uuidv4(),
                    "model": modelName
                })
            })
            if (!resp.ok) {
                const error = await resp.json().catch(() => ({}))
                throw new Error(!error ? JSON.stringify(error) : `${resp.status} ${resp.statusText}`)
            }

            let resData;
            const parser = createParser(event => {
                if (event.data == '[DONE]') {
                    if (JSON.parse(resData).conversation_id) {
                        setConversationProperty(token, JSON.parse(resData).conversation_id, { is_visible: false })
                    }
                    sendResponse({
                        status: 200,
                        message: JSON.parse(resData).message.content.parts[0]
                    });
                } else {
                    resData = event.data;
                }
            });

            for await (const chunk of streamAsyncIterable(resp.body)) {
                const str = new TextDecoder().decode(chunk)
                parser.feed(str)
            }
        }
        return true;
    }
}